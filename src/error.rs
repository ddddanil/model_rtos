use nix::sys::signal;
use thiserror::Error;

use crate::rtos::TaskId;

#[derive(Debug, Error)]
pub enum OsError {
    #[error(transparent)]
    MmapError(#[from] mmap_rs::Error),
    #[error("Error from errno: {} {}: {}", .0, *.0 as i32, .0.desc())]
    HostError(#[from] nix::errno::Errno),
    #[error("OS was already initialized")]
    AlreadyInitError,
    #[error("Missing task with id {0}")]
    MissingTask(TaskId),
    #[error("Duplicated task with id {0}")]
    DuplicatedTask(TaskId),
    #[error("Inconsistent task with id {0}")]
    InconsistentTask(TaskId),
    #[error("Missing IRQ handler for {0}")]
    MissingIrq(signal::Signal),
    #[error("No more work")]
    NoMoreWork,
    #[error("Uninitialized thread")]
    UninitializedThread,
}

pub type Result<T = ()> = std::result::Result<T, OsError>;

pub fn panic_with(error: OsError) -> ! {
    panic!("RTOS kernel panic: {}", error.to_string())
}
