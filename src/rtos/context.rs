use std::{default::default, fmt::Debug, marker::PhantomData, mem};

use crate::error::Result;
use nix::errno::Errno;
use tracing::trace;

use super::stack::Stack;

pub struct Context<'a>(libc::ucontext_t, PhantomData<&'a ()>);

impl<'a> Context<'a> {
    fn as_ptr(&self) -> *const libc::ucontext_t {
        &self.0 as *const libc::ucontext_t
    }

    fn as_mut_ptr(&mut self) -> *mut libc::ucontext_t {
        &mut self.0 as *mut libc::ucontext_t
    }

    pub fn make_context<F>(
        stack: &'a Stack,
        callee: &'a F,
        returning_context: &Context,
    ) -> Result<Self>
    where
        F: ContextCallable,
    {
        trace!("Make context");
        // Shim interface function
        extern "C" fn callee_wrapper<F>(closure: *mut libc::c_void)
        where
            F: ContextCallable,
        {
            let opt_closure = closure as *mut Option<F>;
            unsafe {
                let _res = (*opt_closure).take().unwrap().call(());
            }
        }
        // Callee
        let callee_fn: extern "C" fn() =
            unsafe { mem::transmute(callee_wrapper::<F> as extern "C" fn(_)) };
        let callee_closure = callee as *const F;
        let callee_closure: *mut libc::c_void = unsafe { mem::transmute(callee_closure) }; // *mut F into *mut c_void

        // Context
        let mut new_context = Self::get_context()?;
        new_context.0.uc_stack = stack.as_stack_t();
        new_context.0.uc_link = unsafe { mem::transmute(&returning_context.0) };

        // Make
        let context_ptr = &mut new_context.0 as *mut libc::ucontext_t;
        unsafe { libc::makecontext(context_ptr, callee_fn, 1, callee_closure) };
        Ok(new_context)
    }

    pub fn get_context() -> Result<Self> {
        trace!("Get context");
        let mut context = mem::MaybeUninit::<libc::ucontext_t>::uninit();
        let res = unsafe { libc::getcontext(context.as_mut_ptr()) };
        Errno::result(res)?;
        let context = unsafe { context.assume_init() };
        Ok(Context(context, default()))
    }

    pub fn set_context(&self) -> Result {
        trace!("Set context");
        let context = &self.0;
        let res = unsafe { libc::setcontext(context as *const libc::ucontext_t) };
        Errno::result(res)?;
        Ok(())
    }

    pub fn swap_context(&self, saved_context: &mut Context) -> Result {
        trace!("Swap context");
        let old_context_ptr = &mut saved_context.0 as *mut libc::ucontext_t;
        let new_context_ptr = self.as_ptr();
        let ret = unsafe { libc::swapcontext(old_context_ptr, new_context_ptr) };
        Errno::result(ret)?;
        Ok(())
    }
}

unsafe impl<'c> Sync for Context<'c> {}
unsafe impl<'c> Send for Context<'c> {}

impl<'a> AsRef<libc::ucontext_t> for Context<'a> {
    fn as_ref(&self) -> &libc::ucontext_t {
        &self.0
    }
}

impl<'a> AsMut<libc::ucontext_t> for Context<'a> {
    fn as_mut(&mut self) -> &mut libc::ucontext_t {
        &mut self.0
    }
}

impl<'a> Debug for Context<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Context")
            .field("stack_addr", &self.0.uc_stack.ss_sp)
            .field("stack_size", &self.0.uc_stack.ss_size)
            .field("context_link", &self.0.uc_link)
            .finish()
    }
}

pub trait ContextCallable: Fn() + Sync + Send {}
impl<F> ContextCallable for F where F: Fn() + Sync + Send {}
