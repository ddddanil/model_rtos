use std::cell::{RefCell, UnsafeCell};

use tracing::debug;

use crate::error::{self, OsError, Result};

use super::{
    context::Context,
    processes::LockedProcess,
    rtos::get_rtos,
    syscall::{self, Syscall},
};

thread_local! {
    static SYSCALL_PASS: RefCell<Syscall> = RefCell::new(Syscall::Exit);
    static CURRENT_TASK: UnsafeCell<Option<LockedProcess>> = UnsafeCell::new(Option::None);
    static OS_CONTEXT: UnsafeCell<Context<'static>> = UnsafeCell::new(Context::get_context().expect("make thread-local os context"));
}

pub fn set_syscall(syscall: Syscall) {
    SYSCALL_PASS.set(syscall);
}

pub fn get_thread_os_context() -> &'static mut Context<'static> {
    OS_CONTEXT.with(|os_ctx| unsafe { &mut *os_ctx.get() as &'static mut Context<'static> })
}

pub fn switch_to_task() -> Result {
    debug!("Switch to task");
    let os_ctx = get_thread_os_context();
    CURRENT_TASK.with(|proc| {
        let proc = unsafe { &mut *proc.get() as &mut Option<LockedProcess> };
        if let Some(proc) = proc {
            proc.with_ctx(|proc_ctx| proc_ctx.swap_context(os_ctx))?;
        } else {
            Err(OsError::UninitializedThread)?;
        }
        Ok(())
    })
}

pub fn switch_to_os() -> Result {
    debug!("Switch to OS");
    let os_ctx = get_thread_os_context();
    CURRENT_TASK.with(|proc| {
        let proc = unsafe { &mut *proc.get() as &mut Option<LockedProcess> };
        if let Some(proc) = proc {
            proc.with_ctx_mut(|proc_ctx| os_ctx.swap_context(proc_ctx))?;
        } else {
            Err(OsError::UninitializedThread)?;
        }
        Ok(())
    })
}

pub fn run_os_loop() {
    let rtos = get_rtos();

    let l = || -> Result<!> {
        loop {
            let task_id = rtos
                .scheduler
                .lock()
                .next_candidate()
                .ok_or(OsError::NoMoreWork)?;
            let task = rtos.procs.lock().steal_task(task_id)?;

            CURRENT_TASK.with(|task_ctx| -> Result {
                let task_ctx = unsafe { &mut *task_ctx.get() as &mut Option<LockedProcess> };
                *task_ctx = Some(task);
                switch_to_task()?;
                drop(task_ctx.take());
                Ok(())
            })?;

            let syscall = SYSCALL_PASS.take();
            syscall::execute_syscall(rtos, task_id, syscall)?;
        }
    };

    if let Err(e) = l() {
        if let OsError::NoMoreWork = e {
            return;
        }
        error::panic_with(e)
    } else {
        unreachable!("OS has stopped without an error")
    }
}
