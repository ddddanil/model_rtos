use std::time;

use ouroboros::self_referencing;

use super::context::{Context, ContextCallable};
use super::rtos::{get_api, RtosApi};
use super::stack::Stack;
use crate::error::Result;

#[self_referencing]
pub struct Process {
    stack: Stack,
    callable: Box<dyn ContextCallable>,
    #[borrows(callable, mut stack)]
    #[not_covariant]
    context: Context<'this>,
}

impl Process {
    pub fn create(callable: Box<dyn ContextCallable>, returning_context: &Context) -> Result<Self> {
        let stack = Stack::new()?;
        let proc = ProcessTryBuilder {
            stack,
            callable,
            context_builder: |callable, mapping| {
                Context::make_context(mapping, callable, returning_context)
            },
        }
        .try_build()?;
        Ok(proc)
    }

    pub fn with_ctx<'s, F, R>(&'s self, f: F) -> R
    where
        F: for<'this> FnOnce(&'s Context<'this>) -> R,
    {
        self.with_context(f)
    }

    pub fn with_ctx_mut<'s, F, R>(&'s mut self, f: F) -> R
    where
        F: for<'this> FnOnce(&'s mut Context<'this>) -> R,
    {
        self.with_context_mut(f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem;

    #[test]
    fn check_swap() {
        let callee = || {
            println!("Im on a new stack!");
            let stack_var = Context::get_context().expect("Get ctx");
            let stack_addr: *const libc::c_void =
                unsafe { mem::transmute(stack_var.as_ref() as *const libc::ucontext_t) };
            println!("Page stack: {:p}", stack_addr);
        };

        let mut returning_ctx = Context::get_context().expect("Get ctx");
        let proc = Process::create(Box::new(callee), &returning_ctx).expect("Create task stack");
        proc.with_context(|ctx| {
            Context::swap_context(ctx, &mut returning_ctx).expect("Swap to callee");
        });
        println!("Returned from call");
    }
}

pub trait ProcessTask {
    fn deadline_delay(&self) -> time::Duration;
    fn run(&self, rtos: &RtosApi) -> ();
}

pub type StaticProcessTask = &'static (dyn ProcessTask + Sync + Send);

pub fn process_task_trampoline(task: StaticProcessTask) -> impl ContextCallable {
    || {
        let rtos = get_api();
        task.run(&rtos);
        rtos.exit();
    }
}
