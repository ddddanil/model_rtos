use std::fmt::Debug;

use tracing::instrument;

use crate::Result;

use super::process::{process_task_trampoline, Process, StaticProcessTask};
use super::rtos::Rtos;
use super::semaphore::Semaphore;
use super::task::{TaskEntry, TaskId};
use super::{irq, thread};

#[derive(Default)]
pub enum Syscall {
    SpawnTask(StaticProcessTask),
    AcquireSem(&'static Semaphore),
    ReleaseSem(&'static Semaphore),
    WaitForSignals,
    Yield,
    #[default]
    Exit,
}

impl Debug for Syscall {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::SpawnTask(_) => write!(f, "SpawnTask"),
            Self::AcquireSem(s) => write!(f, "AcquireSem({:p}, {})", s, s.val()),
            Self::ReleaseSem(s) => write!(f, "ReleaseSem({:p}, {})", s, s.val()),
            Self::Yield => write!(f, "Yield"),
            Self::WaitForSignals => write!(f, "WaitForSignals"),
            Self::Exit => write!(f, "Exit"),
        }
    }
}

#[instrument(skip(rtos))]
fn execute_exit(rtos: &Rtos, task_id: TaskId) -> Result {
    rtos.scheduler.lock().remove_task(&task_id)?;
    rtos.procs.lock().remove_task(task_id)?;
    Ok(())
}

#[instrument(skip(rtos))]
fn execute_acquire_sem(rtos: &Rtos, task_id: TaskId, sem: &'static Semaphore) -> Result {
    if let None = sem.try_acquire() {
        rtos.scheduler.lock().set_blocked(&task_id, sem)?;
    };
    Ok(())
}

#[instrument(skip(rtos))]
fn execute_release_sem(rtos: &Rtos, task_id: TaskId, sem: &'static Semaphore) -> Result {
    sem.release();
    rtos.scheduler.lock().choose_unblocked(sem)?;
    Ok(())
}

#[instrument(skip(rtos, task))]
pub fn execute_spawn_task(rtos: &Rtos, task: StaticProcessTask) -> Result {
    let entry = TaskEntry::new_task(task.deadline_delay())?;
    let process = Process::create(
        Box::new(process_task_trampoline(task)),
        &thread::get_thread_os_context(),
    )?;
    rtos.procs.lock().register_task(entry.id, process)?;
    rtos.scheduler.lock().add_task(entry);
    Ok(())
}

#[instrument(skip(rtos))]
fn execute_wait_for_signals(rtos: &Rtos) -> Result {
    let sigset = rtos.irq_handlers.lock().sig_set();
    irq::wait_and_execute_signal(&sigset)?;
    Ok(())
}

pub fn execute_syscall(rtos: &Rtos, task_id: TaskId, syscall: Syscall) -> Result {
    match syscall {
        Syscall::Exit => execute_exit(rtos, task_id),
        Syscall::AcquireSem(s) => execute_acquire_sem(rtos, task_id, s),
        Syscall::ReleaseSem(s) => execute_release_sem(rtos, task_id, s),
        Syscall::Yield => Ok(()),
        Syscall::SpawnTask(task) => execute_spawn_task(rtos, task),
        Syscall::WaitForSignals => execute_wait_for_signals(rtos),
        _ => unimplemented!("Syscall {:?} is not implemented", syscall),
    }
}
