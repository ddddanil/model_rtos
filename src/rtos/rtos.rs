use std::sync::OnceLock;

use spin::Mutex;
use tracing::{info, instrument};

use crate::error::{self, Result};

use super::irq::SignalHandlers;
use super::processes::ProcessList;
use super::scheduler::TaskScheduler;
use super::semaphore::Semaphore;
use super::{syscall, thread, StaticIrqTask, StaticProcessTask};

pub struct Rtos {
    pub scheduler: Mutex<TaskScheduler>,
    pub procs: Mutex<ProcessList>,
    pub irq_handlers: Mutex<SignalHandlers>,
}

pub struct RtosApi(&'static Rtos);

impl RtosApi {
    fn do_syscall(&self, syscall: syscall::Syscall) {
        info!("Syscall: {:?}", syscall);
        thread::set_syscall(syscall);
        if let Err(e) = thread::switch_to_os() {
            error::panic_with(e);
        }
    }

    pub fn acquire_sem(&self, sem: &'static Semaphore) {
        self.do_syscall(syscall::Syscall::AcquireSem(sem))
    }

    pub fn release_sem(&self, sem: &'static Semaphore) {
        self.do_syscall(syscall::Syscall::ReleaseSem(sem))
    }

    pub fn exit(&self) {
        self.do_syscall(syscall::Syscall::Exit)
    }

    pub fn yield_now(&self) {
        self.do_syscall(syscall::Syscall::Yield)
    }

    pub fn wait_for_signals(&self) {
        self.do_syscall(syscall::Syscall::WaitForSignals)
    }

    pub fn add_task(&self, task: StaticProcessTask) {
        self.do_syscall(syscall::Syscall::SpawnTask(task))
    }
}

static OS_INSTANCE: OnceLock<Rtos> = OnceLock::new();

#[instrument(skip_all)]
pub fn run_os(first_task: StaticProcessTask, irqs: &[StaticIrqTask]) -> Result {
    let scheduler = Mutex::new(TaskScheduler::new());
    let procs = Mutex::new(ProcessList::new());
    let irq_handlers = Mutex::new(SignalHandlers::new()?);
    irqs.iter()
        .for_each(|&irq| irq_handlers.lock().register(irq));
    irq_handlers.lock().initialise_irq()?;
    let rtos = Rtos {
        scheduler,
        procs,
        irq_handlers,
    };
    OS_INSTANCE
        .set(rtos)
        .map_err(|_| crate::OsError::AlreadyInitError)?;

    syscall::execute_spawn_task(get_rtos(), first_task)?;
    thread::run_os_loop();
    Ok(())
}

pub fn get_rtos() -> &'static Rtos {
    OS_INSTANCE.get().expect("OS was not initialized")
}

pub fn get_api() -> RtosApi {
    RtosApi(get_rtos())
}
