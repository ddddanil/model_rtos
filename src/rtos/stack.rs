use mmap_rs::{MmapFlags, MmapMut, MmapOptions};

use crate::error::Result;

pub struct Stack(MmapMut);

impl Stack {
    pub fn new() -> Result<Self> {
        let stack = MmapOptions::new(64 * 1024 /*KiB*/)
            .with_flags(MmapFlags::STACK)
            .map_mut()?;
        Ok(Stack(stack))
    }
    pub fn as_stack_t(&self) -> libc::stack_t {
        let ss_sp = self.0.as_ptr() as *mut libc::c_void; // *mut i8 to *mut c_void
        let ss_flags = 0;
        let ss_size = self.0.len();
        libc::stack_t {
            ss_sp,
            ss_flags,
            ss_size,
        }
    }
}

unsafe impl Sync for Stack {}
unsafe impl Send for Stack {}
