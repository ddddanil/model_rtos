use std::sync::atomic::{AtomicUsize, Ordering};

#[derive(Debug)]
pub struct Semaphore(AtomicUsize);

impl Semaphore {
    pub const fn new(sem: usize) -> Self {
        Semaphore(AtomicUsize::new(sem))
    }

    pub fn val(&self) -> usize {
        self.0.load(Ordering::Acquire)
    }

    pub(crate) fn release(&self) {
        self.0.fetch_add(1, Ordering::SeqCst);
    }

    pub(crate) fn try_acquire(&self) -> Option<()> {
        let mut curr = self.0.load(Ordering::Acquire);
        let mut next = if curr == 0 { 0 } else { curr - 1 };
        while let Err(_) = self
            .0
            .compare_exchange(curr, next, Ordering::SeqCst, Ordering::Acquire)
        {
            curr = self.0.load(Ordering::Acquire);
            next = if curr == 0 { 0 } else { curr - 1 };
        }
        if curr == next {
            None
        } else {
            Some(())
        }
    }
}
