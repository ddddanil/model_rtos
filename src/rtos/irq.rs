use std::{
    collections::HashMap,
    default::default,
    mem::{self, MaybeUninit},
    ptr,
};

use nix::{
    errno::Errno,
    sys::signal::{self, SigAction},
};
use parking_lot as pl;
use tracing::{debug, info};

use super::rtos::get_rtos;
use super::stack::Stack;
use crate::error::{OsError, Result};

pub struct SignalHandlers {
    handlers: HashMap<signal::Signal, pl::Mutex<StaticIrqTask>>,
    stack: Stack,
}

impl SignalHandlers {
    pub fn new() -> Result<Self> {
        let stack = Stack::new()?;
        Ok(SignalHandlers {
            handlers: default(),
            stack,
        })
    }

    pub fn register(&mut self, irq_task: StaticIrqTask) {
        info!(
            "Registering IRQ handler for {:?}",
            irq_task.signal_activation().iter().collect::<Vec<_>>()
        );
        irq_task.signal_activation().iter().for_each(|s| {
            self.handlers.insert(s, pl::Mutex::new(irq_task));
        });
    }

    fn get_handler_for(&self, sig: signal::Signal) -> Result<StaticIrqTask> {
        let handler = self
            .handlers
            .get(&sig)
            .ok_or(OsError::MissingIrq(sig))?
            .lock()
            .clone();
        Ok(handler)
    }

    pub fn sig_set(&self) -> signal::SigSet {
        let mut set = signal::SigSet::empty();
        self.handlers.keys().for_each(|&s| set.add(s));
        set
    }

    fn sigaction(&self) -> signal::SigAction {
        SigAction::new(
            sigaction_handler(),
            default_sigaction_flags(),
            self.sig_set(),
        )
    }

    fn sig_stack(&self) -> libc::stack_t {
        self.stack.as_stack_t()
    }

    pub fn initialise_irq(&self) -> Result {
        debug!("Initializing the IRQ system");
        let errno = unsafe { libc::sigaltstack(&self.sig_stack() as *const _, ptr::null_mut()) };
        if errno != 0 {
            Err(Errno::last())?;
        };
        let sigaction = self.sigaction();
        self.handlers.keys().try_for_each(|s| unsafe {
            signal::sigaction(*s, &sigaction)?;
            Ok::<(), OsError>(())
        })?;
        Ok(())
    }
}

extern "C" fn sigaction_wrapper(
    sig: libc::c_int,
    siginfo: *mut libc::siginfo_t,
    ctx: *mut libc::ucontext_t,
) {
    let sig = signal::Signal::try_from(sig).expect("Valid signal number");
    info!("In signal for {}", sig);
    let handler = get_rtos().irq_handlers.lock().get_handler_for(sig).unwrap();
    handler.run(sig);
}

pub fn wait_and_execute_signal(signals: &signal::SigSet) -> Result {
    let (signum, mut siginfo) = wait_for_signal(signals)?;
    sigaction_wrapper(signum, &mut siginfo, ptr::null_mut());
    Ok(())
}

fn wait_for_signal(signals: &signal::SigSet) -> Result<(libc::c_int, libc::siginfo_t)> {
    let mut siginfo = MaybeUninit::<libc::siginfo_t>::uninit();
    let sigset = signals.as_ref();
    let signum =
        unsafe { libc::sigwaitinfo(sigset as *const libc::sigset_t, siginfo.as_mut_ptr()) };
    if signum == -1 {
        Err(Errno::last())?;
    };
    Ok((signum, unsafe { siginfo.assume_init() }))
}

fn sigaction_handler() -> signal::SigHandler {
    signal::SigHandler::SigAction(unsafe {
        mem::transmute(sigaction_wrapper as extern "C" fn(_, _, _))
    })
}

fn default_sigaction_flags() -> signal::SaFlags {
    signal::SaFlags::SA_ONSTACK
}

pub trait IrqTask {
    fn signal_activation(&self) -> signal::SigSet;
    fn run(&self, sig: signal::Signal) -> ();
}

pub type StaticIrqTask = &'static (dyn IrqTask + Sync + Send);
