use std::{
    cmp::Ordering,
    fmt::Display,
    num::NonZeroUsize,
    ops::Add,
    sync::atomic::{self, AtomicUsize},
    time,
};

use crate::error::Result;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TaskId(NonZeroUsize);

impl Display for TaskId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("TaskId").field(&self.0).finish()
    }
}

static TASK_ID_GEN: AtomicUsize = AtomicUsize::new(1);

impl TaskId {
    fn new() -> Self {
        let next_id = TASK_ID_GEN.fetch_add(1, atomic::Ordering::SeqCst);
        TaskId(NonZeroUsize::try_from(next_id).expect("Non zero task id generation"))
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum TaskState {
    Runnable,
    Blocked(usize),
}

#[derive(Debug)]
pub struct TaskEntry {
    pub id: TaskId,
    pub deadline: time::Instant,
    pub state: TaskState,
}

impl TaskEntry {
    pub fn new_task(delay: time::Duration) -> Result<Self> {
        let id = TaskId::new();
        let deadline = time::Instant::now().add(delay);
        let state = TaskState::Runnable;
        let entry = Self {
            id,
            deadline,
            state,
        };
        Ok(entry)
    }

    pub fn cmp_by_id(&self, other: &Self) -> Ordering {
        self.id.cmp(&other.id)
    }

    pub fn cmp_by_deadline(&self, other: &Self) -> Ordering {
        self.deadline.cmp(&other.deadline)
    }
}

#[derive(Debug)]
pub struct TaskEntryByDeadline(pub TaskEntry);

impl PartialEq for TaskEntryByDeadline {
    fn eq(&self, other: &Self) -> bool {
        self.0.id == other.0.id
    }
}
impl Eq for TaskEntryByDeadline {}
impl PartialOrd for TaskEntryByDeadline {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.deadline.partial_cmp(&other.0.deadline)
    }
}
impl Ord for TaskEntryByDeadline {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0
            .cmp_by_deadline(&other.0)
            .then(self.0.cmp_by_id(&other.0))
    }
}

impl AsRef<TaskEntry> for TaskEntryByDeadline {
    fn as_ref(&self) -> &TaskEntry {
        &self.0
    }
}
