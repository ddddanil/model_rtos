use std::collections::HashMap;
use std::sync::Arc;

use parking_lot as pl;
use parking_lot::lock_api;
use tracing::instrument;

use super::process::Process;
use super::TaskId;
use crate::error::{OsError, Result};

pub struct ProcessList {
    procs: HashMap<TaskId, Arc<pl::Mutex<Process>>>,
}

pub type LockedProcess = lock_api::ArcMutexGuard<pl::RawMutex, Process>;

impl ProcessList {
    pub fn new() -> Self {
        ProcessList {
            procs: HashMap::default(),
        }
    }

    #[instrument(skip(self, task))]
    pub fn register_task(&mut self, task_id: TaskId, task: Process) -> Result {
        let task = Arc::new(pl::Mutex::new(task));
        if let Some(_) = self.procs.insert(task_id, task.clone()) {
            Err(crate::OsError::DuplicatedTask(task_id))?;
        };
        Ok(())
    }

    #[instrument(skip(self))]
    pub fn steal_task(&self, task_id: TaskId) -> Result<LockedProcess> {
        let task = self
            .procs
            .get(&task_id)
            .ok_or(OsError::MissingTask(task_id))?;
        Ok(task.lock_arc())
    }

    #[instrument(skip(self))]
    pub fn remove_task(&mut self, task_id: TaskId) -> Result {
        let _ = self
            .procs
            .remove(&task_id)
            .ok_or(OsError::MissingTask(task_id))?;
        Ok(())
    }
}
