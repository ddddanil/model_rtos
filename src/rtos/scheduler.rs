use std::collections::BTreeSet;

use tracing::{info, instrument};

use super::{
    semaphore::Semaphore,
    task::{TaskEntry, TaskEntryByDeadline, TaskId, TaskState},
};
use crate::{error::Result, OsError};

pub struct TaskScheduler {
    process_set: BTreeSet<TaskEntryByDeadline>,
}

impl<'c> TaskScheduler {
    pub fn new() -> Self {
        Self {
            process_set: BTreeSet::new(),
        }
    }

    #[instrument(skip(self))]
    pub fn add_task(&mut self, task: TaskEntry) {
        let has_inserted = self.process_set.insert(TaskEntryByDeadline(task));
        if !has_inserted {
            panic!("Attempt to add an existing task")
        }
    }

    #[instrument(skip(self))]
    pub fn next_candidate(&mut self) -> Option<TaskId> {
        self.process_set
            .iter()
            .map(|entry| &entry.0)
            .find(|entry| entry.state == TaskState::Runnable)
            .map(|entry| entry.id)
    }

    #[instrument(skip(self))]
    pub fn set_blocked(&mut self, task_id: &TaskId, sem: *const Semaphore) -> Result<()> {
        let task = self
            .process_set
            .iter()
            .find(|entry| &entry.0.id == task_id)
            .ok_or(OsError::MissingTask(*task_id))?;
        let new_task = TaskEntry {
            state: TaskState::Blocked(sem as usize),
            ..(*task).0
        };
        let old_task = self.process_set.replace(TaskEntryByDeadline(new_task));
        assert!(old_task.is_some());
        Ok(())
    }

    #[instrument(skip(self))]
    pub fn choose_unblocked(&mut self, sem: *const Semaphore) -> Result<()> {
        let task = self
            .process_set
            .iter()
            .find(|entry| &entry.0.state == &TaskState::Blocked(sem as usize));
        if let Some(task) = task {
            info!("Found task to unblock: {:?}", task.0.id);
            let new_task = TaskEntry {
                state: TaskState::Runnable,
                ..(*task).0
            };
            let old_task = self.process_set.replace(TaskEntryByDeadline(new_task));
            assert!(old_task.is_some());
        }
        Ok(())
    }

    #[instrument(skip(self))]
    pub fn remove_task(&mut self, task_id: &TaskId) -> Result<()> {
        let c = self
            .process_set
            .drain_filter(|entry| &entry.0.id == task_id)
            .count();
        match c {
            0 => Err(OsError::MissingTask(*task_id)),
            1 => Ok(()),
            _ => Err(OsError::DuplicatedTask(*task_id)),
        }
    }
}
