#![feature(default_free_fn)]
#![feature(unboxed_closures)]
#![feature(fn_traits)]
#![feature(never_type)]
#![feature(btree_drain_filter)]
#![feature(local_key_cell_methods)]

mod error;
mod rtos;

pub use self::rtos::{run_os, IrqTask, ProcessTask, RtosApi as Rtos, Semaphore};
pub use error::{OsError, Result};
