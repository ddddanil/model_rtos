use rtos::run_os;
use rtos_macro::task;
use tracing::info;

#[task(deadline_ms = 800.0)]
fn task_parent(rtos: &Rtos) {
    info!("Running task_parent");
    rtos.add_task(&task_later);
    rtos.add_task(&task_earlier);
}

#[task(deadline_ms = 200.0)]
fn task_earlier(rtos: &Rtos) {
    info!("Running task_earlier");
}

#[task(deadline_ms = 500.0)]
fn task_later(rtos: &Rtos) {
    info!("Running example task")
}

fn main() -> color_eyre::Result<()> {
    tracing_subscriber()?;
    run_os(&task_parent, &[])?;
    Ok(())
}

pub fn tracing_subscriber() -> color_eyre::Result<()> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::fmt;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::EnvFilter;

    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    color_eyre::install()?;

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "trace")
    }

    tracing_subscriber::Registry::default()
        .with(EnvFilter::from_default_env())
        .with(fmt::layer())
        .with(ErrorLayer::default())
        .init();
    Ok(())
}
