use core::time;

use nix::sys::signal;
use rtos::run_os;
use rtos_macro::{irq, task};
use tracing::info;

#[task(deadline_ms = 800.0)]
fn task_parent(rtos: &Rtos) {
    info!("Running task_parent");
    signal::raise(signal::SIGUSR1).unwrap();
    std::thread::sleep(time::Duration::from_millis(300));
    rtos.wait_for_signals();
}

#[irq(signals=[signal::SIGUSR1])]
fn example_irq(signal: signal::Signal) {
    info!("Running example irq: {}", signal)
}

fn main() -> color_eyre::Result<()> {
    tracing_subscriber()?;

    run_os(&task_parent, &[&example_irq])?;
    info!("Post Rtos");
    Ok(())
}

pub fn tracing_subscriber() -> color_eyre::Result<()> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::fmt;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::EnvFilter;

    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    color_eyre::install()?;

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "trace")
    }

    tracing_subscriber::Registry::default()
        .with(EnvFilter::from_default_env())
        .with(fmt::layer())
        .with(ErrorLayer::default())
        .init();
    Ok(())
}
