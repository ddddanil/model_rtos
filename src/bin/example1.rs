use rtos::run_os;
use rtos_macro::task;
use tracing::info;

#[task(deadline_ms = 200.0)]
fn task1(rtos: &Rtos) {
    info!("Running task1");
    rtos.add_task(&task2)
}

#[task(deadline_ms = 200.0)]
fn task2(rtos: &Rtos) {
    info!("Running task2");
    rtos.add_task(&example_task)
}

#[task(deadline_ms = 200.0)]
fn example_task(rtos: &Rtos) {
    info!("Running example task")
}

fn main() -> color_eyre::Result<()> {
    tracing_subscriber()?;
    run_os(&task1, &[])?;
    info!("Post Rtos");
    Ok(())
}

pub fn tracing_subscriber() -> color_eyre::Result<()> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::fmt;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::EnvFilter;

    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    color_eyre::install()?;

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "trace")
    }

    tracing_subscriber::Registry::default()
        .with(EnvFilter::from_default_env())
        .with(fmt::layer())
        .with(ErrorLayer::default())
        .init();
    Ok(())
}
