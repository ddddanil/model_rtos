use rtos::{run_os, Semaphore};
use rtos_macro::task;
use tracing::{info, instrument};

static SEM: Semaphore = Semaphore::new(1);

#[task(deadline_ms = 800.0)]
#[instrument(skip_all)]
fn task_parent(rtos: &Rtos) {
    info!("Running task_parent");
    rtos.acquire_sem(&SEM);
    info!("Acquired sem");
    rtos.add_task(&task_child);
    rtos.release_sem(&SEM);
    info!("Released sem");
}

#[task(deadline_ms = 200.0)]
#[instrument(skip_all)]
fn task_child(rtos: &Rtos) {
    info!("Running task_child");
    rtos.acquire_sem(&SEM);
    info!("Acquired sem");
    rtos.release_sem(&SEM);
    info!("Released sem");
}

fn main() -> color_eyre::Result<()> {
    tracing_subscriber()?;
    run_os(&task_parent, &[])?;
    Ok(())
}

pub fn tracing_subscriber() -> color_eyre::Result<()> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::fmt;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::EnvFilter;

    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    color_eyre::install()?;

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "trace")
    }

    tracing_subscriber::Registry::default()
        .with(EnvFilter::from_default_env())
        .with(fmt::layer())
        .with(ErrorLayer::default())
        .init();
    Ok(())
}
