use darling::{export::NestedMeta, Error, FromMeta};
use proc_macro::TokenStream;
use quote::quote;
use syn::{spanned::Spanned, Ident, ItemFn};

#[derive(Debug, FromMeta)]
struct TaskArgs {
    deadline_ms: f64,
}

#[proc_macro_attribute]
pub fn task(args: TokenStream, input: TokenStream) -> TokenStream {
    let args = match NestedMeta::parse_meta_list(args.into()) {
        Ok(a) => a,
        Err(e) => {
            return TokenStream::from(Error::from(e).write_errors());
        }
    };
    let input = syn::parse_macro_input!(input as ItemFn);
    let args = match TaskArgs::from_list(&args) {
        Ok(v) => v,
        Err(e) => {
            return TokenStream::from(e.write_errors());
        }
    };

    let vis = &input.vis;
    let fn_name = &input.sig.ident;
    let mod_name = Ident::new(&format!("__{}", fn_name), input.span());
    let struct_name = Ident::new(&format!("s__{}", fn_name), input.span());
    let static_name = Ident::new(&fn_name.to_string().to_uppercase(), input.span());
    let fn_block = &input.block;
    let deadline = args.deadline_ms;

    let output = quote! {
        mod #mod_name {
            use super::*;
            #[allow(non_camel_case_types)]
            pub struct #struct_name {}
            impl ::rtos::ProcessTask for #struct_name {
                fn deadline_delay(&self) -> ::std::time::Duration {
                    ::std::time::Duration::from_secs_f64(#deadline / 1000.0)
                }

                fn run<'a>(&self, rtos: &'a ::rtos::Rtos) #fn_block
            }
            pub static #static_name: #struct_name = #struct_name {};
        }
        #vis use #mod_name::#static_name as #fn_name;
    };
    output.into()
}

#[derive(Debug, FromMeta)]
struct IrqArgs {
    signals: syn::Expr,
}

#[proc_macro_attribute]
pub fn irq(args: TokenStream, input: TokenStream) -> TokenStream {
    let args = match NestedMeta::parse_meta_list(args.into()) {
        Ok(a) => a,
        Err(e) => {
            return TokenStream::from(Error::from(e).write_errors());
        }
    };
    let input = syn::parse_macro_input!(input as ItemFn);
    let args = match IrqArgs::from_list(&args) {
        Ok(v) => v,
        Err(e) => {
            return TokenStream::from(e.write_errors());
        }
    };

    let vis = &input.vis;
    let fn_name = &input.sig.ident;
    let mod_name = Ident::new(&format!("__{}", fn_name), input.span());
    let struct_name = Ident::new(&format!("s__{}", fn_name), input.span());
    let static_name = Ident::new(&fn_name.to_string().to_uppercase(), input.span());
    let fn_block = &input.block;
    let signals = args.signals;

    let output = quote! {
        mod #mod_name {
            use super::*;
            #[allow(non_camel_case_types)]
            pub struct #struct_name {}
            impl ::rtos::IrqTask for #struct_name {
                fn signal_activation(&self) -> ::nix::sys::signal::SigSet {
                    ::nix::sys::signal::SigSet::from_iter(#signals)
                }

                fn run(&self, signal: signal::Signal) #fn_block
            }
            pub static #static_name: #struct_name = #struct_name {};
        }
        #vis use #mod_name::#static_name as #fn_name;
    };
    output.into()
}
