#import "template.typ": *

#show: project.with(
  type: "Курсовая работа",
  name: "Модель операционной системы реального времени",
  subject: "«Архитектура программных систем»",
  authors: (
    (
    name: "Дорошин Д.А.", 
    group: "3530904/00104",
    tutor: "Коликова Т. В."
    ),
  ),
  // date
  date: "May 30, 2023",
)

#show raw.where(block: true): block.with(
  width: 100%,
  fill: luma(240),
  inset: 10pt,
  radius: 4pt,
  stroke: black,
  breakable: true
)
#show figure.where(kind: "code"): figure.with(supplement: "Листинг")

#set par(
  first-line-indent: 1em,
  justify: true,
)

#set heading(numbering: "1.")

#include "task.typ"
#include "project.typ"
#include "tests.typ"


