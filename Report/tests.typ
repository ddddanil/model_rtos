#import "template.typ": code
= Тесты
== Тест 1 -- `Scheduler`
#figure(
    code[
    ```rs
#[task(deadline_ms = 800.0)]
fn task_parent(rtos: &Rtos) {
    info!("Running task_parent");
    rtos.add_task(&task_later);
    rtos.add_task(&task_earlier);
}

#[task(deadline_ms = 200.0)]
fn task_earlier(rtos: &Rtos) {
    info!("Running task_earlier");
}

#[task(deadline_ms = 500.0)]
fn task_later(rtos: &Rtos) {
    info!("Running example task")
}

fn main() -> color_eyre::Result<()> {
    run_os(&task_parent, &[])?;
    Ok(())
}
    ```
], kind: "code", supplement: "Листинг",
 caption: [Содержание теста],
)

Этот тест проверяет семантику работы планировщика. В каждой ситуации он должен 
выбирать процесс с ближайшим дедлайном.

Ход работы теста
#table(
    columns: (auto, auto),
    align: left,
    [Активированные задачи], [Выбранная задача],
    [`task_parent` -- 800 мс], [*`task_parent`*],
    [`task_parent` -- 800 мс, `task_later` -- 500 мс], [*`task_later`*],
    [`task_parent` -- 800 мс], [*`task_parent`*],
    [`task_parent` -- 800 мс, `task_earlier` -- 200 мс], [*`task_earlier`*],
    [`task_parent` -- 800 мс], [*`task_parent`*],
)

#page(height: auto, [
#figure(
    code[
    ```tcl
2023-06-01T07:25:55.391544Z DEBUG run_os: rtos::rtos::irq: Initializing the IRQ system
2023-06-01T07:25:55.391559Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:25:55.391566Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Make context
2023-06-01T07:25:55.391569Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:25:55.391583Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:25:55.391586Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391589Z  INFO run_os: example_edf::__task_parent: Running task_parent
2023-06-01T07:25:55.391591Z  INFO run_os: rtos::rtos::rtos: Syscall: SpawnTask
2023-06-01T07:25:55.391595Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:25:55.391596Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391602Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Make context
2023-06-01T07:25:55.391604Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:25:55.391611Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:25:55.391613Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391615Z  INFO run_os: example_edf::__task_later: Running example task
2023-06-01T07:25:55.391617Z  INFO run_os: rtos::rtos::rtos: Syscall: Exit
2023-06-01T07:25:55.391619Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:25:55.391622Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391628Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:25:55.391630Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391632Z  INFO run_os: rtos::rtos::rtos: Syscall: SpawnTask
2023-06-01T07:25:55.391634Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:25:55.391635Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391640Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Make context
2023-06-01T07:25:55.391642Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:25:55.391650Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:25:55.391653Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391655Z  INFO run_os: example_edf::__task_earlier: Running task_earlier
2023-06-01T07:25:55.391656Z  INFO run_os: rtos::rtos::rtos: Syscall: Exit
2023-06-01T07:25:55.391658Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:25:55.391661Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391666Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:25:55.391668Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:25:55.391670Z  INFO run_os: rtos::rtos::rtos: Syscall: Exit
2023-06-01T07:25:55.391672Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:25:55.391674Z TRACE run_os: rtos::rtos::context: Swap context
    ```
], kind: "code", supplement: "Листинг",
 caption: [Вывод теста],
)
])

== Тест 2 -- `IRQ`
#figure(
    code[
    ```rs
#[task(deadline_ms = 800.0)]
fn task_parent(rtos: &Rtos) {
    info!("Running task_parent");
    signal::raise(signal::SIGUSR1).unwrap();
    std::thread::sleep(time::Duration::from_millis(300));
    rtos.wait_for_signals();
}

#[irq(signals=[signal::SIGUSR1])]
fn example_irq(signal: signal::Signal) {
    info!("Running example irq: {}", signal)
}

fn main() -> color_eyre::Result<()> {
    run_os(&task_parent, &[&example_irq])?;
    Ok(())
}
    ```
], kind: "code", supplement: "Листинг",
 caption: [Содержание теста],
)

Этот тест проверяет семантику работы прерываний. Объявляется обработчик для 
сигнала *`SIGUSR1`*. Первый раз этот сигнал вызывается из процесса. Потом процесс 
ожидает данного сигнала, который будет получен от пользователя. Сигнал генерируется
командой #underline(stroke: 1.5pt+red, offset: 3pt, raw(lang: "bash", "pkill -SIGUSR1 example_irq"))

#page(height: auto, [
#figure(
    code[
    ```tcl
2023-06-01T07:36:08.450464Z  INFO run_os: rtos::rtos::irq: Registering IRQ handler for [SIGUSR1]
2023-06-01T07:36:08.450472Z DEBUG run_os: rtos::rtos::irq: Initializing the IRQ system
2023-06-01T07:36:08.450477Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:36:08.450483Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Make context
2023-06-01T07:36:08.450486Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:36:08.450501Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:36:08.450504Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:36:08.450506Z  INFO run_os: example_irq::__task_parent: Running task_parent
2023-06-01T07:36:08.450515Z  INFO run_os: rtos::rtos::irq: In signal for SIGUSR1
2023-06-01T07:36:08.450518Z  INFO run_os: example_irq::__example_irq: Running example irq: SIGUSR1
2023-06-01T07:36:08.750587Z  INFO run_os: rtos::rtos::rtos: Syscall: WaitForSignals
2023-06-01T07:36:08.750617Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:36:08.750622Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:36:13.457883Z  INFO run_os:execute_wait_for_signals: rtos::rtos::irq: In signal for SIGUSR1
2023-06-01T07:36:13.457899Z  INFO run_os:execute_wait_for_signals: example_irq::__example_irq: Running example irq: SIGUSR1
2023-06-01T07:36:13.457919Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:36:13.457924Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:36:13.457931Z  INFO run_os: rtos::rtos::rtos: Syscall: Exit
2023-06-01T07:36:13.457936Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:36:13.457939Z TRACE run_os: rtos::rtos::context: Swap context
    ```
], kind: "code", supplement: "Листинг",
 caption: [Вывод теста],
)
])

== Тест 3 -- Semaphore
#figure(
    code[
    ```rs
static SEM: Semaphore = Semaphore::new(1);

#[task(deadline_ms = 800.0)]
#[instrument(skip_all)]
fn task_parent(rtos: &Rtos) {
    info!("Running task_parent");
    rtos.acquire_sem(&SEM);
    info!("Acquired sem");
    rtos.add_task(&task_child);
    rtos.release_sem(&SEM);
    info!("Released sem");
}

#[task(deadline_ms = 200.0)]
#[instrument(skip_all)]
fn task_child(rtos: &Rtos) {
    info!("Running task_child");
    rtos.acquire_sem(&SEM);
    info!("Acquired sem");
    rtos.release_sem(&SEM);
    info!("Released sem");
}

fn main() -> color_eyre::Result<()> {
    run_os(&task_parent, &[])?;
    Ok(())
}
    ```
], kind: "code", supplement: "Листинг",
 caption: [Содержание теста],
)

Этот тест проверяет семантику работы семафора. Объявлен один семафор с единичным
счётчиком (мьютекс). Две задачи раздельно им пользуются

Ход работы теста
#table(
    columns: (auto, auto, auto),
    align: left,
    [Состояние `task_parent`], [Состояние `task_child`], [Состояние семофора],
    [Active], [Inactive], [1],
    [Active], [Inactive], [0 (*`task_parent`*)],
    [Active], [Active], [0 (*`task_parent`*)],
    [Active], [Blocked], [0 (*`task_parent`*)],
    [Active], [Blocked], [1],
    [Inactive], [Active], [1],
    [Inactive], [Active], [0 (*`task_child`*)],
    [Inactive], [Active], [1],
    [Inactive], [Inactive], [1],
)

#page(height: auto, [
#figure(
    code[
    ```tcl
2023-06-01T07:47:23.589075Z DEBUG run_os: rtos::rtos::irq: Initializing the IRQ system
2023-06-01T07:47:23.589084Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:47:23.589091Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Make context
2023-06-01T07:47:23.589094Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:47:23.589108Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:47:23.589131Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589135Z  INFO run_os: example_sem::__task_parent: Running task_parent
2023-06-01T07:47:23.589137Z  INFO run_os: rtos::rtos::rtos: Syscall: AcquireSem(0x7f9384624ee8, 1)
2023-06-01T07:47:23.589142Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:47:23.589158Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589166Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:47:23.589168Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589170Z  INFO run_os: example_sem::__task_parent: Acquired sem
2023-06-01T07:47:23.589172Z  INFO run_os: rtos::rtos::rtos: Syscall: SpawnTask
2023-06-01T07:47:23.589174Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:47:23.589176Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589187Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Make context
2023-06-01T07:47:23.589189Z TRACE run_os:execute_spawn_task: rtos::rtos::context: Get context
2023-06-01T07:47:23.589199Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:47:23.589200Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589203Z  INFO run_os: example_sem::__task_child: Running task_child
2023-06-01T07:47:23.589205Z  INFO run_os: rtos::rtos::rtos: Syscall: AcquireSem(0x7f9384614ee8, 0)
2023-06-01T07:47:23.589208Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:47:23.589210Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589216Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:47:23.589218Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589220Z  INFO run_os: rtos::rtos::rtos: Syscall: ReleaseSem(0x7f9384624ee8, 0)
2023-06-01T07:47:23.589222Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:47:23.589224Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589227Z  INFO run_os:execute_release_sem{task_id=TaskId(1) sem=Semaphore(0)}:choose_unblocked{sem=0x55ab1e4b9058}: rtos::rtos::scheduler: Found task to unblock: TaskId(2)
2023-06-01T07:47:23.589233Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:47:23.589235Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589237Z  INFO run_os: example_sem::__task_child: Acquired sem
2023-06-01T07:47:23.589238Z  INFO run_os: rtos::rtos::rtos: Syscall: ReleaseSem(0x7f9384614ee8, 1)
2023-06-01T07:47:23.589240Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:47:23.589242Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589247Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:47:23.589249Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589251Z  INFO run_os: example_sem::__task_child: Released sem
2023-06-01T07:47:23.589253Z  INFO run_os: rtos::rtos::rtos: Syscall: Exit
2023-06-01T07:47:23.589254Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:47:23.589256Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589262Z DEBUG run_os: rtos::rtos::thread: Switch to task
2023-06-01T07:47:23.589264Z TRACE run_os: rtos::rtos::context: Swap context
2023-06-01T07:47:23.589266Z  INFO run_os: example_sem::__task_parent: Released sem
2023-06-01T07:47:23.589268Z  INFO run_os: rtos::rtos::rtos: Syscall: Exit
2023-06-01T07:47:23.589269Z DEBUG run_os: rtos::rtos::thread: Switch to OS
2023-06-01T07:47:23.589271Z TRACE run_os: rtos::rtos::context: Swap context
    ```
], kind: "code", supplement: "Листинг",
 caption: [Вывод теста],
)
])