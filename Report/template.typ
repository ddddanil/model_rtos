// The project function defines how your document looks.
// It takes your content and some metadata and formats it.
// Go ahead and customize it to your liking!
#let project(
  type: "",
  name: "",
  subject: "",
  abstract: [],
  authors: (),
  date: none,
  logo: none,
  bibliography-file: none,
  body,
) = {
  // Set the document's basic properties.
  set document(author: authors.map(a => a.name), title: type)
  set page(
    margin: (left: 30mm, right: 10mm, top: 20mm, bottom: 20mm),
    numbering: none, //"1",
    number-align: center,
  )
  set text(font: "Times New Roman", lang: "ru", size: 12pt) // font does not work
  show math.equation: set text(weight: 400)

  // Title page.
  //v(0.0fr)
  align(center)[
    #text(1.25em, weight: 100, "Санкт-Петербургский политехнический университет Петра Великого") \
    #text(1.25em, weight: 100, "Институт компьютерных наук и технологий") \
    #text(1.25em, weight: 100, "Высшая школа программной инженерии")
  ]

  v(6cm)

  align(center)[
    #text(2em, weight: 100, type) \
    #text(1.25em, weight: 100, name)
  ]

  v(0.5cm)

  align(center)[
    #text(1.25em, weight: 100, "по дисциплине") \
    #text(1.25em, weight: 100, subject)
  ]

  v(0.1cm)

  //align(center)[
  //  text(1.75em, weight: 100, title2)
  //]

  //Author information.
  v(1fr)
  pad(
    // top: 0.7em,
    
    grid(
      gutter: 27em,
      columns: 1,
      ..authors.map(author => align(left)[
        #text("Выполнил: ") #h(1fr) #text(1.1em, author.name) \
        #text("Группа:") #h(1fr) #text(1.1em, author.group) \
        \
        #text("Проверил: ") #h(1fr) #text(1.1em, author.tutor)
      ]),
    ),
  )

  v(1fr)

  align(center)[
    #text(1.25em, weight: 200, "Санкт-Петербург \n 2023")
  ]

  pagebreak()

  // Table of contents.
  outline(depth: 3, indent: true, title: "Содержание:")
  pagebreak()

  // Main body.
  set par(justify: true)
  set page(numbering: "1", number-align: center)

  body

}

#let code(
  body,
  line-numbers: true,
  row-gutter: 10pt,
  column-gutter: 10pt,
  inset: 10pt,
  fill: rgb(98%, 98%, 98%),
  stroke: 1pt + rgb(50%, 50%, 50%)
) = {
  set par(justify: false)
  let content = ()
  let i = 1

  for item in body.children {
    if item.func() == raw {
      for line in item.text.split("\n") {
        if line-numbers {
          content.push(raw(str(i)))
        }

        content.push(raw(line, lang: item.lang))
        i += 1
      }
    }
  }

  align(left, block(
    stroke: stroke,
    inset: inset,
    fill: fill,
    width: 100%,
    breakable: true
  )[
    #table(
      columns: if line-numbers {2} else {1},
      inset: 0pt,
      stroke: none,
      fill: none,
      row-gutter: row-gutter,
      column-gutter: column-gutter,
      align:
        if line-numbers {
          (x, _) => (right, left).at(x)
        } else {
          left
        }
      ,
      ..content
    )
  ])
}